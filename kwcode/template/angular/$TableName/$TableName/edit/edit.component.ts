import { Component, OnInit, ViewChild } from '@angular/core';
import { SFSchema, SFUISchema, SFRadioWidgetSchema } from '@delon/form';
import { _HttpClient } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { API } from 'src/app/shared/api';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-$tableName-edit',
  templateUrl: './edit.component.html',
})
export class $TableNameEditComponent implements OnInit {
  record: any = {};
  i: any;
  schema: SFSchema = {
    properties: {
    $foreach
    ${[NotMap(Id,CreationTime,CreatorUserId,LastModificationTime,LastModifierUserId,IsDeleted,DeleterUserId,DeletionTime,TenantId)]
      $TableColumnName: { type: 'string', title: '$TableColumnCNName', maxLength: 140, },
    $}
    },
    // 必填项
    required: ['carNumber'],
  };
  ui: SFUISchema = {
    '*': {
      spanLabelFixed: 100,
      grid: { span: 12 },
    },
    $maintainMethod: {
      widget: 'textarea',
      grid: { span: 24 },
      autosize: { minRows: 3, maxRows: 10 },
    },
    $enterTime: {
      widget: 'date',
    },
    $leaveTime: {
      widget: 'date',
    },
    $changePart: {
      widget: 'textarea',
      grid: { span: 24 },
      autosize: { minRows: 3, maxRows: 10 },
    },
    $remark: {
      widget: 'textarea',
      grid: { span: 24 },
      autosize: { minRows: 3, maxRows: 10 },
    },
  };

  constructor(private modal: NzModalRef, private msgSrv: NzMessageService, public http: _HttpClient) {}

  /**
   * 初始化时查询
   */
  ngOnInit(): void {
    if (this.record.id > 0) {
      this.http.get(API.$TableNameGet, { id: this.record.id }).subscribe((res) => (this.i = res.result));
    } else {
      this.i = {};
    }
  }

  /**
   * 保存
   */
  save(value: any) {
    console.log(this.record.id);
    if (this.record.id === 0) {
      const url = API.$TableNameCreate;
      this.http.post(url, value).subscribe((res) => {
        this.msgSrv.success('新建成功');
        this.modal.close(true);
      });
    } else {
      const url = API.$TableNameUpdate;
      this.http.put(url, value).subscribe((res) => {
        this.msgSrv.success('更新成功');
        this.modal.close(true);
      });
    }
  }

  close() {
    this.modal.destroy();
  }
}
